var gulp = require('gulp');
var concat = require('gulp-concat');
var minify = require('gulp-minify');

gulp.task( "libraries", function () {
	return gulp.src([
		'node_modules/jquery/dist/jquery.min.js',
		'node_modules/lodash/lodash.min.js',
		'node_modules/bootstrap/dist/js/bootstrap.min.js',
		'node_modules/angular/angular.js',
	])
	.pipe( concat( 'libraries.js') )
	.pipe( gulp.dest( './dist/') );
});

gulp.task( "styles", function() {
	return gulp.src([
		'node_modules/normalize.css/normalize.css',
		'node_modules/bootstrap/dist/css/bootstrap.min.css',
	])
	.pipe( concat( 'styles.css' ) )
	.pipe( gulp.dest( './dist/') );
});

gulp.task( "scripts", function () {
	return gulp.src([
		'app/app.js',
		'app/js/services/LocalService.js',
		'app/js/services/CitiesDataService.js',
		'app/js/controllers/CitiesController.js',
		'app/js/directives/CityForecastDirective.js',
		'app/js/controllers/MainController.js'
	])
	.pipe( concat( 'app.js') )
	.pipe( gulp.dest( './dist/'));
});

gulp.task( "minify", function () {
	gulp.src('dist/app.js')
	.pipe( minify({
		ext:{
			min: '.min.js'
		}
	}))
	.pipe( gulp.dest('dist') )
});

gulp.task( "build", [ 'libraries', 'scripts', 'styles', 'minify' ]);
