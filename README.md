# POPEYE #
#### AngularJS - Weather forecast app
###### ( [https://en.wikipedia.org/wiki/Operation_Popeye](https://en.wikipedia.org/wiki/Operation_Popeye) )

Popeye is a single-page webapp designed for view on mobile devices. Users can view the current conditions int heir current city, as well as a small list of selected favorite cities.


## Getting Started
Some quick setup is required to properly run Popeye

1. From the root folder run `npm install` to install packages.
2. then run `gulp build` to create compiled files.
3. And thats it!

Popeye can now be run by simply opening the index.html in the browser, no need for a server.

## Features of Popeye

After being welcomed the app, users will be presented to choose their current city ( for the sake of this excercise I chose 24 of the most popular European cities ).

User will then be presented with a small list of cities and their current temperatures, icons designating the current weather conditions, and wind speed. 

- Both the Current City and Favorite Cities list can be updated in the Settings menu.
- All city choices, and changes are saved in local storage for the user's next visit.

Each city listed can be clicked/tapped to expand to a full hourly forecast and further details about the current weather conditions ( wind speed, humidity, and Highs & Lows )

In addition to changing the Current and Favorite cities, user can also switch between Metric and Imperial temperature units.

## Archetecture

The app is broken down into a couple Controllers, a main Directive, and a couple of Services.

#### MainController:
- Handles the initial load and setting of city information
- Handles settings changes, and saving to local storage

#### CitiesController:
- Handles fetching basic forecast data for the group of cities ( current, and favorites )

#### CityForecastDirective:
- Reusable directive for each city to display basic forecast information, as well as attaching an expanded window with hourly and detailed information.

#### LocalService:
- Small service module for saving and retreiving local storage values

#### CitiesDataService:
- Static data factory to feed a list of available cities with id and names form OpenWeatherMap's api, as well as a small default cities list.

### Other Libraries used:
- LoDash - utility library
- Bootstrap - CSS and JS framework for layouts
- LinearIcons - icon font library


## Possible future improvements

- Add Unit testing suite
- Add seperated and cleaner templating files.
- Create simple routes for each window/page 
	- templates and routes were removed to prevent browser errors when attempting to serve files from file://