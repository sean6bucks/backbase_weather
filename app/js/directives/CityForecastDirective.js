'use strict'

popeye.directive( 'cityForecast', [ '$timeout', '$compile', function( $timeout, $compile ) {
	return {
		restrict: 'E',
		replace: false,
		// USING TEMPLATE INSIDE OF DIRECTIVE TO AVOID BROWSER ERRORS LOADING TEMPLATE FROM :file//
		template:
			'<article class="panel city-panel" ng-click="openHourlyForecast()">' +
				'<div class="sk_loader" ng-if="city.loading">' +
					'<div class="loader small"></div>' +
				'</div>' +
				'<div class="city-info">' +
					'<span class="lnr lnr-{{ city.icon }}"></span>' +
					'<h1>{{ city.name }}</h1>' +
				'</div>' +
				'<div class="city-temp" ng-show="city.main.temp > -1">' +
					'<p class="temp-degrees text-right">' +
						'{{ city.main.temp | number : 0 }}&#176;' +
					'</p>' +
					'<p class="temp-wind subtext text-center">' +
						'wind: {{ city.wind.speed | msToKmh | number : 0 }} {{ options.units == "metric" ? "km/h" : "mph" }}' +
					'</p>' +
				'</div>' +
			'</article>',
		scope: {
			city: '=',
			options: '='
		},
		controller: [ '$scope', '$http', '$filter', function ($scope, $http, $filter ) {

			$scope.base_url = 'https://api.openweathermap.org/data/2.5/';
			$scope.api_key = 'e0dc4630f4742b122d9647247ccdf6df';
			$scope.units = $scope.options.units || 'metric';
			$scope.hourly_pos = 0;

			// HUE ARRAY FOR HOURLYS
			var hues = [ '#101c48', '#0b1330', '#050918', '#050918', '#0b1330', '#101c48', '#162660', '#1b3078', '#213990', '#2643a8', '#2c4cc0', '#3156d8', '#3760f0', '#3156d8', '#2c4cc0', '#2643a8', '#213990', '#1b3078', '#162660', '#101c48', '#0b1330', '#050918', '#0b1330', '#101c48' ];

			function timeToHue( time ) {
				var hour_str = $filter('date')( time * 1000, "H" );
				var hour = _.toInteger( hour_str );
				return hues[hour];
			};

			function setWeatherIcon( code ) {
				if ( ( code >= 200 && code < 250 ) || ( code >= 801 && code < 810 ) ) {
					return 'cloud';
				} else if ( code >= 300 && code < 550 ) {
					return 'drop';
				} else {
					return 'sun';
				}
			};

			// FETCH DATA FOR TRI-HOURLY FORECAST
			$scope.getForecast = function( callback ) {
				var city_id = $scope.city.id;
				$http.get( $scope.base_url + 'forecast', {
					params: {
						id: city_id,
						APPID: $scope.api_key,
						units: $scope.units
					}
				})
				.then(
					function( response ){
						$scope.city.hourly = _.map( response.data.list, function(item) {
							return {
								// FORMAT TO MILLISECONDS FOR ANGULAR FILTER
								dt: item.dt * 1000,
								dt_txt: item.dt_txt,
								hue: timeToHue( item.dt ),
								icon: setWeatherIcon( item.weather[0].id ),
								temp: item.main.temp
							};
						}).splice( 0, 15 ) /* SPLICE DOWN TO 48 HOUR CHUNK */;

						if ( _.isFunction( callback ) ) {
							callback();
						}
					},
					function( errors ) {
						console.log( 'ERROR: ', errors );
					}
				).finally( function() {
					$scope.loadingState = false;
				});
			};

		}],
		link: function( scope, element, attr ) {
			if ( !scope.city || !scope.city.id ) return;

			scope.openHourlyForecast = function() {
				// IF NO HOURLY DATA FETCHED FOR CITY > GET HOURLY FORECAST
				if ( !scope.city.hourly ) {
					// ADD LOADING SHADE
					scope.city.loading = true;
					// FETCH HOURLY DATA
					scope.getForecast( 
						function(){
							appendForecastPanel();
							scope.city.loading = false;
						}
					);
					return;
				}
				// ELESE OPEN PANEL
				appendForecastPanel();
			};

			scope.closeHourlyForecast = function() {
				removeForecastPanel();
			};

			scope.shiftHourly = function( dir ) {
				var item_w = scope.element_width;
				var cycle_w = 4 * item_w;
				var max_left = ( 3 * cycle_w ) * -1;

				if ( dir == 'left' ) {
					if ( scope.hourly_pos >= 0 ) return;
					scope.hourly_pos += cycle_w;
				} else {
					if ( scope.hourly_pos <= max_left ) return;
					scope.hourly_pos -= cycle_w;
				}
			};

			// ===========

			function appendForecastPanel() {
				// BUILD AND ATTACH FORECAST OVERLAY
				var template = angular.element(
					'<div id="forecast-{{ city.id }}" class="panel forecast-panel" ng-class="{ open: panel_open, closing: panel_closing }">' +
						'<span class="close-x" ng-click="closeHourlyForecast()"></span>' +
						'<div class="overview-forecast background-{{ city.icon }} text-center">' +
							'<span class="background-icon lnr lnr-{{ city.icon }}"></span>' +
							'<div class="overview-info">' +
								'<h1 class="text-center">{{ city.main.temp | number : 0 }}&#176;</h1>' +
								'<h2 class="text-center">{{ city.name }}</h2>' +
								'<ul>' +
									'<li>Wind speed: {{ city.wind.speed | msToKmh | number : 1 }}{{ options.units == "metric" ? "km/h" : "mph" }}</li>' +
									'<li>Humidity: {{ city.main.humidity }}%</li>' +
									'<li>High: {{ city.main.temp_max }}&#176; / Low: {{ city.main.temp_min }}&#176;</li>' +
								'</ul>' +
							'</div>' +
						'</div>' +
						'<div class="hourly-forecast">' +
							'<span class="hourly-arrow text-center" ng-click="shiftHourly(\'left\')">' +
								'<span class="lnr lnr-chevron-left"></span>' +
							'</span>' +
							'<div class="hourly-list" ng-style="{ left: hourly_pos + \'px\' }">' +
								'<span class="hourly-item" ng-style="{ background: city.hourly[0].hue, width: element_width }"></span>' +
								'<span class="hourly-item text-center" ng-repeat="item in city.hourly track by $index" ng-style="{ background: item.hue, width: element_width }">' +
									'<p>{{ item.dt | date : "H:mm" }}</p>' +
									'<p class="hourly-temp">{{ item.temp | number : 0 }}&#176;</p>' +
									'<p><span class="lnr lnr-{{ item.icon }}"></span></p>' +
								'</span>' +
								'<span class="hourly-item" ng-style="{ background: city.hourly[city.hourly.length - 1].hue, width: element_width }"></span>' +
							'</div>' +
							'<span class="hourly-arrow text-center" ng-click="shiftHourly(\'right\')">' +
								'<span class="lnr lnr-chevron-right"></span>' +
							'</span>' +
						'</div>' +
					'</div>'
				);
				element.append( template[0] );
				$compile(template)(scope);
				// GET PANEL WIDTH FOR ACCURATE SCROLL
				scope.element_width = getPanelWidth();

				// AFTER ATTACHING FORECAST OVERLAY > TRIGGER OPEN
				scope.panel_open = true;
			}

			function removeForecastPanel() {
				scope.panel_open = false;
				$timeout( function(){
					angular.element( '#forecast-' + scope.city.id ).remove();
				}, 400 );
			}

			function getPanelWidth() {
				var list_el = angular.element('.hourly-forecast');
				var width = list_el[0].offsetWidth;
				return width / 5;
			}
		}
	}
}]);
