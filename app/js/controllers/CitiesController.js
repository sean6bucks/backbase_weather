'use strict'

popeye.controller( 'CitiesController', [ '$scope', '$rootScope', '$http', '$timeout', 'LocalService', 'CITIES_DATA', function( $scope, $rootScope, $http, $timeout, LocalService, CITIES_DATA ) {

	// INTERNAL FUNCTIONS ==========

	function getForecasts( options ) {
		self.loading_data = true;
        // GET API BASE URL
		var api_url = $scope.api_base_url;
        // CREATE CITIES ARRAY
		var cities = _.concat( [$scope.current_city], $scope.active_cities );
		var city_ids = _.map( cities, function( city ) {
			return city.id
		});
        // UPDATE ENPOINT IF MULTIPLE CITY DATA NEEDED
		var endpoint = city_ids.length > 1 ? 'group' : 'weather';

        // CALL API FOR FRECAST DATA
		return $http.get( api_url + endpoint, {
			params: {
				id: city_ids.join(','),
				APPID: $scope.api_key,
				units: options.units || 'metric'
			}
		})
		.then(
			function( response ) {
                // IF GROUP DATA IS FETCHED > USE list VALUE
				if ( city_ids.length > 1 ) {
					var data = response.data.list;
                // ELSE SEND ARRAY OF SINGLE CITY TO SEARCH ARRAY
				} else {
					var data = [response.data];
				}
				// UPDATE CITY DATA
				updateCities( data );
				// REMOVE ANY LOADING SHADES
				self.initial_load = self.loading_data = false;
			},
			function( errors ) {
				console.log( 'ERROR: ', errors );
                // TODO: HANDLE ERRORS
			}
		)
	}

	function setWeatherIcon( code ) {
		if ( ( code >= 200 && code < 250 ) || ( code >= 801 && code < 810 ) ) {
			return 'cloud';
		} else if ( code >= 300 && code < 550 ) {
			return 'drop';
		} else {
			return 'sun';
		}
	};

	function updateCities( data ) {
		// UPDATE FORECAST DATA ON CURRENT CITY
		_.extend( $scope.current_city, _.find( data, { id: $scope.current_city.id } ) );
		// ADD ICON TYPE TO CITY
		$scope.current_city.icon = setWeatherIcon( $scope.current_city.weather[0].id );

		// UPDATE DATA FOR ACTIVE CITIES LIST
		_.forEach( $scope.active_cities, function( city ) {
			_.extend( city, _.find( data, { id: city.id }));
			city.icon = setWeatherIcon( city.weather[0].id );
		});
	}

	// DOM CONNECTED FUNCTIONS ==========

	var self = this;

	self.initCities = function() {
		// ADD LOADING SHADE
		self.initial_load = true;
        // GET SETTING OPTIONS IF AVAILABLE
		var options = $scope.settings || {};
        // FETCH FORECASTS DATA
		getForecasts( options );
	};

	// LISTEN FOR UDATED SETTINGS TO RE-LOAD FORECASTS
	$scope.$on( 'updateSettings', function( event, data ) {
		$scope.current_city = data.current_city;
		$scope.active_cities = data.active_cities;
		getForecasts( data.settings );
	});

}]);

// FILTER FOR m/s to km/h

popeye.filter( 'msToKmh', function(){
	return function(val) {
		return val * 3.6;
	};
});
