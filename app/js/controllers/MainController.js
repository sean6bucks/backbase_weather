'use strict'

popeye.controller( 'MainController', [ '$scope', '$rootScope', '$http', '$timeout', 'LocalService','CITIES_DATA', function( $scope, $rootScope, $http, $timeout, LocalService, CITIES_DATA ) {

    // CREATE ANY DEFAULT SETTINGS FOR MAIN SCOPE

	$scope.api_base_url = 'https://api.openweathermap.org/data/2.5/';
	$scope.api_key = 'e0dc4630f4742b122d9647247ccdf6df';
	$scope.loading_app = true;
	$scope.settings = {
		units: 'metric'
	};

	// INTERNAL FUNCTIONS =============
	function setAvailableCities() {
        // FETCH CITIES LIST FOR SELECTION
		self.available_cities = _.sortBy( CITIES_DATA.cities, 'name' );
	}

	function toggleWelcomeScreen() {
		self.welcome_open = self.welcome_open == true ? false : true;
	}

	function removeWelcomeScreen() {
		$timeout( function(){ 
			angular.element('#welcome-block').remove();
		});
	}

	function setCurrentCity( city, save ) {
        // SET CURRENT CITY TO MAIN SCOPE
		$scope.current_city = city;
        // SAVE TO LOCAL IF NEW SELECTION AND CLOSE WELCOME
		if ( save ) saveLocal( 'popeye_current_city', city );
	}

	function getActiveCities() {
		// IF LOCAL ACTIVE CITIES > RETURN
		var active_cities = LocalService.getData( 'popeye_active_cities' );
		if ( active_cities ) return active_cities;
		// ELSE RETURN DEFAULTS
		return setDefaultCities();
	}

	function setDefaultCities() {
		var defaults = CITIES_DATA.default_active;
		// IF CURRENT CITY IS ONE OF DEFAULTS > REMOVE FROM DEFAULT LIST
		if ( _.find( defaults, { id: $scope.current_city.id } ) ) {
			return _.filter( defaults, function( city ) {
				return city.id !=  $scope.current_city.id;
			});
		// ELSE JUST FEED FIRST FOUR CITIES
		} else {
			return defaults.splice(0,4);
		}
	}

	function saveLocal( key, data ) {
		LocalService.saveLocal( key, data );
	}

	// DOM CONNECTED FUNCTIONS ===========

	var self = this;

	// INIT USER
	self.init = function() {
		// SET CITIES LIST FOR WELCOME AND SETTINGS
		setAvailableCities();
		// CHECK LOCAL STORAGE FOR CITY SETTING
		var current_city = LocalService.getData( 'popeye_current_city' );
        // IF CURRENT CITY IN LOCAL STORAGE SET TO SCOPE
		if ( current_city && current_city.id ) {
			setCurrentCity( current_city, false );
			// CHECK LOCAL FOR ACTIVE CITIES > IF NONE SET DEFAULTS
			$scope.active_cities = getActiveCities();
			saveLocal( 'popeye_active_cities', $scope.active_cities );
			// REMOVE WELCOME DOM ELEMENT
			removeWelcomeScreen();
			return;
		}
		// ELSE SHOW WELCOME SCREEN
		toggleWelcomeScreen();
	};

	self.selectCurrentCity = function( city ) {
        // SET CURRENT CITY TO MAIN SCOPE AND SAVE TO LOCAL
		setCurrentCity( city, true );
		// SET DEFAULT ACTIVE CITIES
		$scope.active_cities = setDefaultCities();
        // SAVE TO LOCAL IF NEW SELECTION AND CLOSE WELCOME
		toggleWelcomeScreen();
		$timeout( function() {
			removeWelcomeScreen();
		}, 400 );
	};

	self.updateCurrentCity = function( city ) {
		setCurrentCity( city, true );
		// LET closeSettings KNOW TO REFETCH DATA
		self.setting_changed = true;
	};

	self.updateActiveCity = function( city, index ) {
		console.log( index );
		// UPDATE SPECIFIC INDEX OF ACTIVE CITIES LIST
		$scope.active_cities[index] = city;
		// SAVE UPDATED LIST
		saveLocal( 'popeye_active_cities', $scope.active_cities );
		// LET closeSettings KNOW TO REFETCH DATA
		self.setting_changed = true;
	}

	// UPDATE UNIT SETTINGS
	self.setUnits = function( type ) {
		$scope.settings.units = type;
		// LET closeSettings KNOW TO REFETCH DATA
		self.setting_changed = true;
	}

	// CLOSE SETTINGS PANEL AND REFRESH FORECASTS
	self.closeSettings = function() {
		if ( self.setting_changed ) {
			$rootScope.$broadcast( 'updateSettings', {
				current_city: $scope.current_city,
				active_cities: $scope.active_cities,
				settings: $scope.settings
			});
			self.setting_changed = false;
		}
		self.settings_open = false;
	}

}]);
