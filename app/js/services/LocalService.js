'use strict'

popeye.factory( 'LocalService', function() {
	return {
        // CHECK FOR LOCAL DATA UNDER SPECIFIC KEY
		getData: function( keyname ) {
			var local_city = localStorage.getItem( keyname );
			if ( !local_city ) return null;
			return JSON.parse( local_city );
		},
		// ADD AND REMOVE USER DATA TO LOCAL FOR QUICK LOGIN
		saveLocal: function( key, attrs ) {
			if ( attrs ) {
				localStorage.setItem( key, JSON.stringify( attrs ) );
			}
		},
		// REMOVE ON LOGOUT
		deleteLocal: function( key ) {
			localStorage.removeItem( key );
		}
	};
});
