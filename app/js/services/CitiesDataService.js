'use strict'

popeye.service( 'CITIES_DATA', function () {
	var CITIES_DATA = {
		cities: [
			{
				"id": 745044,
				"name": "Istanbul",
				"country": "TR",
				"coord": {
				  "lon": 28.949659,
				  "lat": 41.01384
				}
			},
			{
				"id": 524901,
				"name": "Moscow",
				"country": "RU",
				"coord": {
					"lon": 37.615555,
					"lat": 55.75222
				}
			},
			{
				"id": 2643743,
				"name": "London",
				"country": "GB",
				"coord": {
					"lon": -0.12574,
					"lat": 51.50853
				}
			},
			{
				"id": 2950159,
				"name": "Berlin",
				"country": "DE",
				"coord": {
					"lon": 13.41053,
					"lat": 52.524368
				}
			},
			{
				"id": 6359304,
				"name": "Madrid",
				"country": "ES",
				"coord": {
					"lon": -3.68275,
					"lat": 40.489349
				}
			},
			{
				"id": 703448,
				"name": "Kiev",
				"country": "UA",
				"coord": {
					"lon": 30.516666,
					"lat": 50.433334
				}
			},
			{
				"id": 4219762,
				"name": "Rome",
				"country": "US",
				"coord": {
					"lon": -85.164673,
					"lat": 34.257038
				}
			},
			{
				"id": 6455259,
				"name": "Paris",
				"country": "FR",
				"coord": {
					"lon": 2.35236,
					"lat": 48.856461
				}
			},
			{
				"id": 683506,
				"name": "Bucharest",
				"country": "RO",
				"coord": {
					"lon": 26.10626,
					"lat": 44.432251
				}
			},
			{
				"id": 2761369,
				"name": "Vienna",
				"country": "AT",
				"coord": {
					"lon": 16.37208,
					"lat": 48.208488
				}
			},
			{
				"id": 3054643,
				"name": "Budapest",
				"country": "HU",
				"coord": {
					"lon": 19.039909,
					"lat": 47.498009
				}
			},
			{
				"id": 2911298,
				"name": "Hamburg",
				"country": "DE",
				"coord": {
					"lon": 10,
					"lat": 53.549999
				}
			},
			{
				"id": 756135,
				"name": "Warsaw",
				"country": "PL",
				"coord": {
					"lon": 21.01178,
					"lat": 52.229771
				}
			},
			{
				"id": 3128760,
				"name": "Barcelona",
				"country": "ES",
				"coord": {
					"lon": 2.15899,
					"lat": 41.38879
				}
			},
			{
				"id": 6542283,
				"name": "Milan",
				"country": "IT",
				"coord": {
					"lon": 9.19199,
					"lat": 45.464161
				}
			},
			{
				"id": 3067696,
				"name": "Prague",
				"country": "CZ",
				"coord": {
					"lon": 14.42076,
					"lat": 50.088039
				}
			},
			{
				"id": 2800866,
				"name": "Brussels",
				"country": "BE",
				"coord": {
					"lon": 4.34878,
					"lat": 50.850449
				}
			},
			{
				"id": 2655603,
				"name": "Birmingham",
				"country": "GB",
				"coord": {
					"lon": -1.89983,
					"lat": 52.481419
				}
			},
			{
				"id": 2618425,
				"name": "Copenhagen",
				"country": "DK",
				"coord": {
					"lon": 12.56553,
					"lat": 55.675941
				}
			},
			{
				"id": 2759794,
				"name": "Amsterdam",
				"country": "NL",
				"coord": {
					"lon": 4.88969,
					"lat": 52.374031
				}
			},
			{
				"id": 2747891,
				"name": "Rotterdam",
				"country": "NL",
				"coord": {
					"lon": 4.47917,
					"lat": 51.922501
				}
			},
			{
				"id": 6453366,
				"name": "Oslo",
				"country": "NO",
				"coord": {
					"lon": 10.73367,
					"lat": 59.911831
				}
			},
			{
				"id": 2673730,
				"name": "Stockholm",
				"country": "SE",
				"coord": {
					"lon": 18.064899,
					"lat": 59.332581
				}
			},
			{
				"id": 2964574,
				"name": "Dublin",
				"country": "IE",
				"coord": {
					"lon": -6.26719,
					"lat": 53.34399
				}
			},
		],
		default_active: [
			{
				"id": 2759794,
				"name": "Amsterdam",
				"country": "NL",
				"coord": {
					"lon": 4.88969,
					"lat": 52.374031
				}
			},
			{
				"id": 2643743,
				"name": "London",
				"country": "GB",
				"coord": {
					  "lon": -0.12574,
					  "lat": 51.50853
				}
			},
			{
				"id": 2950159,
				"name": "Berlin",
				"country": "DE",
				"coord": {
					"lon": 13.41053,
					"lat": 52.524368
				}
			},
			{
				"id": 2988506,
				"name": "Paris",
				"country": "FR",
				"coord": {
					"lon": 2.34864,
					"lat": 48.85339
				}
			},
			{
				"id": 2964574,
				"name": "Dublin",
				"country": "IE",
				"coord": {
				  "lon": -6.26719,
				  "lat": 53.34399
				}
			},
		]
	};

	return CITIES_DATA;
});
